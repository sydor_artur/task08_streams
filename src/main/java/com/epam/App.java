package com.epam;

import com.epam.controller.Controller;
import com.epam.model.StreamOfNumbers;
import com.epam.model.StreamWithText;
import com.epam.model.command.Command;
import com.epam.model.command.CommandGo;
import com.epam.model.command.RemoteControl;
import com.epam.model.command.Stuedent;
import com.epam.view.MyViewNumbers;
import com.epam.view.MyViewText;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static final Logger logger = LogManager.getLogger("InfoForUser");

    public static void main(String[] args) {

        //test first part of task
        MyFuncInterface max = (a, b, c) -> Math.max(Math.max(a, b), c);
        MyFuncInterface average = (a, b, c) -> (a + b + c) / 3;
        logger.info(max.func(2, 6, 19));
        logger.info(average.func(2, 6, 19));

        //test second part of task
        RemoteControl remoteControl = new RemoteControl();
        switch (args[0]) {
            case "commandWithLambda":
                Command commandWithLambda = arg -> logger.info("Command with lambda" + arg);
                remoteControl.pressButtom(commandWithLambda, args[1]);
                break;
            case "methodReference":
                Stuedent st = new Stuedent();
                remoteControl.pressButtom(st::run, args[1]);
                break;
            case "anonymousClass":
                Command commandWithAnonymous = new Command() {
                    @Override
                    public void execute(String arg) {
                        logger.info("Command with anonymous class" + arg);
                    }
                };
                commandWithAnonymous.execute(args[1]);
                break;
            case "objectOfCommandClass":
                remoteControl.pressButtom(new CommandGo(new Stuedent()), args[1]);
                break;
            default:
                logger.info("Invalid input");
        }
        //test third part of task
        StreamOfNumbers streamOfNumbers = new StreamOfNumbers();
        Controller controllerNumber = new Controller(streamOfNumbers);
        new MyViewNumbers(controllerNumber).show();

        //test fourth part of task
        StreamWithText streamWithText = new StreamWithText();
        Controller controllerText = new Controller(streamWithText);
        new MyViewText(controllerText).show();
    }
}
