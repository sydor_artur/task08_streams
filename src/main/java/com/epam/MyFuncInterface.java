package com.epam;

@FunctionalInterface
public interface MyFuncInterface {
    int func(int a, int b, int c);
}
