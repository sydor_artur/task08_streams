package com.epam.controller;

import com.epam.model.StreamOfNumbers;
import com.epam.model.StreamWithText;


import java.util.List;
import java.util.Map;

public class Controller {
    private StreamWithText streamWithText;
    private StreamOfNumbers streamOfNumbers;

    public Controller(StreamOfNumbers streamOfNumbers) {
        this.streamOfNumbers = streamOfNumbers;
    }

    public Controller(StreamWithText streamWithText) {
        this.streamWithText = streamWithText;
    }

    public List<Integer> getListOfNumbers() {
        return streamOfNumbers.getIntegersB(10);
    }

    public void showStatistic(List<Integer> list) {
        streamOfNumbers.showStatistic(list);
    }

    public long sum(List<Integer> list) {
        return streamOfNumbers.countSumWithSum(list);
    }

    public long numberOfBiggerThanAverage(List<Integer> list) {
        return streamOfNumbers.biggerThanAverage(list);
    }

    public List<String> takeInputTextFromConsole() {
        return streamWithText.takeUserInput();
    }

    public long numberOfUniqueWords() {
        return streamWithText.countNumberOfUniqueWords();
    }

    public List<String> sort() {
        return streamWithText.sortedListOfUniqueWords();
    }

    public Map<String, Long> occurrenceOfWord() {
        return streamWithText.countOccurrenceOfWord();
    }

    public Map<String, Long> occurrenceOfCharacter() {
        return streamWithText.countOccurrenceOfChar();
    }
}
