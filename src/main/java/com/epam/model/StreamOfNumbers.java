package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamOfNumbers {
    private static final Logger logger = LogManager.getLogger("InfoForUser");


    public List<Integer> getIntegersG(int lim) {
        return Stream.generate(new Random()::nextInt)
                .limit(lim)
                .collect(Collectors.toList());
    }

    public List<Integer> getIntegersB(int lim) {
        Stream.Builder<Integer> stream = Stream.builder();
        for (int i = 0; i < lim; i++) {
            int rand = new Random().nextInt();
            stream
                    .add(rand);
        }
        return stream.build().collect(Collectors.toList());
    }

    public void showStatistic(List<Integer> list) {
        IntSummaryStatistics stat = list.stream()
                .mapToInt(n -> n)
                .summaryStatistics();
        logger.info(stat);
    }

    public long countSumWithSum(List<Integer> list) {
        return list.stream()
                .mapToInt(n -> n)
                .sum();
    }

    public long countSumWithReduce(List<Integer> list) {
        return list.stream()
                .mapToInt(n -> n)
                .reduce(0, Integer::sum);
    }

    public long biggerThanAverage(List<Integer> list) {
        IntSummaryStatistics stat = list.stream()
                .mapToInt(n -> n)
                .summaryStatistics();
        return list.stream()
                .mapToInt(n -> n)
                .filter(n -> n > stat.getAverage())
                .count();
    }
}
