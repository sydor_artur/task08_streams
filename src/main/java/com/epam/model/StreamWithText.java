package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamWithText {
    private static final Logger logger = LogManager.getLogger("InfoForUser");
    private List<String> userText;
    private Scanner scanner = new Scanner(System.in);

    public List<String> takeUserInput() {
        String text;
        userText = new ArrayList<>();
        logger.info("Write some text\n");
        do {
            text = scanner.nextLine();
            userText.add(text);
        } while (!text.equals(""));
        return userText;
    }

    public long countNumberOfUniqueWords() {
        if (userText != null) {
            return userText.stream()
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .filter(s -> !s.equals(""))
                    .distinct()
                    .count();
        }
        return 0;
    }

    public List<String> sortedListOfUniqueWords() {
        if (userText != null) {
            return userText.stream()
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .filter(s -> !s.equals(""))
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());
        }
        return null;
    }

    public Map<String, Long> countOccurrenceOfWord() {
        if (userText != null) {
            return userText.stream()
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .map(String::toLowerCase)
                    .filter(s -> !s.equals(""))
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        }
        return null;
    }

    public Map<String, Long> countOccurrenceOfChar() {
        if (userText != null) {
            return userText.stream()
                    .flatMap(line -> Arrays.stream(line.split(" ")))
                    .flatMap(line -> Arrays.stream(line.split("")))
                    .filter(s -> !s.equals(""))
                    .filter(s -> {
                        if (!((65 <= s.charAt(0)) && (s.charAt(0) <= 90))) {
                            return true;
                        }
                        return false;
                    })
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        }
        return null;
    }
}
