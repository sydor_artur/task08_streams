package com.epam.model.command;

public interface Command {
    void execute(String arg);
}
