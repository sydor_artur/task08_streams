package com.epam.model.command;

public class CommandGo implements Command {
    private final String COMMAND_NAME = "CommandGo";
    private Stuedent stuedent;

    public CommandGo(Stuedent stuedent) {
        this.stuedent = stuedent;
    }

    @Override
    public void execute(String arg) {
        stuedent.go(arg);
    }
}
