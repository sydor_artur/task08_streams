package com.epam.model.command;

public class RemoteControl {
    private Command command;

    void setCommand(Command command) {
        this.command = command;
    }

    public void pressButton(String arg) {
        command.execute(arg);
    }

    public void pressButtom(Command command, String arg) {
        command.execute(arg);
    }
}
