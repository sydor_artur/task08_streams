package com.epam.model.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Stuedent {
    private static final Logger logger = LogManager.getLogger("InfoForUser");

    public void go(String arg) {
        logger.info("Student is going..." + arg);
    }

    public void run(String arg) {
        logger.info("Student is running..." + arg);
    }
}
