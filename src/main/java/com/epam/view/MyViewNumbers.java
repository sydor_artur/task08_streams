package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class MyViewNumbers implements View {
    private static final Logger logger = LogManager.getLogger("InfoForUser");
    private Controller controller;

    public MyViewNumbers(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void show() {
        List<Integer> list = controller.getListOfNumbers();
        list.forEach(logger::info);
        controller.showStatistic(list);
        logger.info("Sum = " + controller.sum(list));
        logger.info("Number bigger than average:" + controller.numberOfBiggerThanAverage(list));
    }
}
