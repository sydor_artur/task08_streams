package com.epam.view;

import com.epam.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class MyViewText implements View {
    private static final Logger logger = LogManager.getLogger("InfoForUser");
    private Controller controller;

    public MyViewText(Controller controller) {
        this.controller = controller;
    }

    @Override
    public void show() {
        controller.takeInputTextFromConsole();
        logger.info("Number of unique words: " + controller.numberOfUniqueWords());
        controller.sort().forEach(logger::info);
        logger.info(controller.occurrenceOfWord());
        logger.info(controller.occurrenceOfCharacter());
    }
}
