package com.epam.view;

import java.util.List;

public interface View {
    public void show();
}
